import { baseApiUrl } from "common";
import { omit } from "lodash";
import { Communication, Company, Contact } from "types";

const companyApiUrl = baseApiUrl + "/Company";
const contactApiUrl = baseApiUrl + "/Contact";
const communicationApiUrl = baseApiUrl + "/Communication";

const get = async (url: string) => {
  return await fetch(url).then((response) => response.json());
};

const post = async (url: string, body?: unknown) => {
  return await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: body ? JSON.stringify(body) : undefined,
  }).then((response) => response.json());
};

const del = async (url: string) => {
  return await fetch(url, {
    method: "DELETE",
  }).then((response) => response.text());
};

//Companies
export const getCompanies = async (id: string[] = []) => {
  return await post(companyApiUrl + "/getCompaniesById", id);
};

export const addCompany = async (company: Company) => {
  const body = omit(company, ["id", "creationTime", "modificationTime"]);
  console.log("Add", body);
  return await post(companyApiUrl + "/create", body);
};

export const deleteCompany = async (id: string) => {
  console.log("Delete", id);
  return del(companyApiUrl + "/delete/" + id);
};

export const updateCompany = async (company: Company) => {
  const body = omit(company, ["creationTime", "modificationTime"]);
  console.log("Save", body);
  return await post(companyApiUrl + "/update", body);
};

//Contacts
export const getContacts = async (id: string[] = []) => {
  return await post(contactApiUrl + "/getContactsById/", id);
};

export const getContactsByCompany = async (companyId: string) => {
  return await post(contactApiUrl + "/getContactsCompanyById/" + companyId);
};

export const addContact = async (contact: Contact) => {
  const body = omit(contact, ["id", "creationTime", "modificationTime"]);
  console.log("Add", body);
  return await post(contactApiUrl + "/create", body);
};

export const updateContact = async (contact: Contact) => {
  const body = omit(contact, ["creationTime", "modificationTime"]);
  console.log("Save", body);
  return await post(contactApiUrl + "/update", body);
};

export const deleteContact = async (id: string) => {
  console.log("Delete", id);
  return del(contactApiUrl + "/delete/" + id);
};

//Communications
export const getCommunications = async (id: string[] = []) => {
  return await post(communicationApiUrl + "/getCommunicationsById", id);
};

export const getCommunicationsByCompany = async (companyId: string) => {
  return await get(
    communicationApiUrl + "/GetCommunicationsByCompanyId/" + companyId,
  );
};

export const addCommunication = async (communication: Communication) => {
  const body = omit(communication, ["id"]);
  console.log("Add", body);
  return await post(communicationApiUrl + "/create", body);
};

export const updateCommunication = async (communication: Communication) => {
  console.log("Save", communication);
  return await post(communicationApiUrl + "/update", communication);
};

export const deleteCommunication = async (id: string) => {
  console.log("Delete", id);
  return del(communicationApiUrl + "/delete/" + id);
};
