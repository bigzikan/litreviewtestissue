import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Switch,
  TextField,
} from "@mui/material";
import {
  communicationsListAtom,
  companiesListAtom,
  editingContactAtom,
  contactsListAtom,
  selectedCompanyAtom,
} from "atoms";
import { FC, useEffect, useState } from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import {
  addContact,
  deleteContact,
  getCommunicationsByCompany,
  getContactsByCompany,
  updateContact,
} from "service";
import { Contact } from "types";

export const ContactsForm: FC = () => {
  const [editedEmployee, setEditedEmployee] =
    useRecoilState(editingContactAtom);
  const [contact, setContact] = useState<Contact | undefined>();
  const companies = useRecoilValue(companiesListAtom);
  const company = useRecoilValue(selectedCompanyAtom);
  const setContactsList = useSetRecoilState(contactsListAtom);
  const setCommunicationsList = useSetRecoilState(communicationsListAtom);

  useEffect(() => {
    setContact(editedEmployee);
  }, [editedEmployee]);

  if (!contact || !company) return null;

  const isEditing = !!contact.creationTime;

  const handleUpdateContacts = () => {
    getContactsByCompany(company.id).then((c) => setContactsList(c));
    getCommunicationsByCompany(company.id).then((c) =>
      setCommunicationsList(c),
    );
  };

  const handleClose = () => {
    setEditedEmployee(undefined);
  };

  const handleDelete = () => {
    deleteContact(contact.id).then(handleUpdateContacts);
    setEditedEmployee(undefined);
  };

  const handleSave = () => {
    isEditing
      ? updateContact(contact).then(handleUpdateContacts)
      : addContact(contact).then(handleUpdateContacts);

    setEditedEmployee(undefined);
  };

  const allowSave =
    !!contact.name &&
    !!contact.jobTitle &&
    !!contact.surname &&
    !!contact.companyId;

  const title = !isEditing
    ? "Добавление сотрудника"
    : `Редактирование "${contact.surname} ${contact.name} ${contact.middleName}"`;

  return (
    <Dialog open={true} onClose={handleClose} maxWidth="xs">
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <TextField
          id="surname"
          required
          autoFocus
          size="small"
          margin="dense"
          label="Фамилия"
          fullWidth
          variant="outlined"
          value={contact.surname}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setContact({ ...contact, surname: event.target.value });
          }}
        />
        <TextField
          id="name"
          required
          size="small"
          margin="dense"
          label="Имя"
          fullWidth
          variant="outlined"
          value={contact.name}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setContact({ ...contact, name: event.target.value });
          }}
        />
        <TextField
          id="middleName"
          margin="dense"
          size="small"
          label="Отчество"
          fullWidth
          variant="outlined"
          value={contact.middleName}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setContact({ ...contact, middleName: event.target.value });
          }}
        />

        <TextField
          id="jobTitle"
          margin="dense"
          size="small"
          label="Должность"
          required
          fullWidth
          variant="outlined"
          value={contact.jobTitle}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setContact({ ...contact, jobTitle: event.target.value });
          }}
        />

        {isEditing ? (
          <FormControl fullWidth margin="dense" size="small">
            <InputLabel id="company-label">Компания</InputLabel>
            <Select
              labelId="company-label"
              id="company"
              size="small"
              label="Компания"
              required
              fullWidth
              value={contact.companyId}
              onChange={(event: SelectChangeEvent) => {
                setContact({
                  ...contact,
                  companyId: event.target.value,
                });
              }}
            >
              {companies.map((company) => (
                <MenuItem key={company.id} value={company.id}>
                  {company.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        ) : null}

        <FormControlLabel
          control={
            <Switch
              checked={contact.isDecisionMaker}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setContact({
                  ...contact,
                  isDecisionMaker: event.target.checked,
                });
              }}
            />
          }
          label="ЛПР"
        />
      </DialogContent>
      <DialogActions sx={{ mr: 2, ml: 2, mb: 2 }}>
        {isEditing && (
          <Button variant="text" color="error" onClick={handleDelete}>
            Удалить
          </Button>
        )}
        <Box sx={{ flex: "auto" }}></Box>
        <Button variant="text" onClick={handleClose}>
          Отмена
        </Button>
        <Button variant="contained" onClick={handleSave} disabled={!allowSave}>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
};
