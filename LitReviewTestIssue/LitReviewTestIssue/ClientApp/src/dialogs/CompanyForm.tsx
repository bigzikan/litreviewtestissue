import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from "@mui/material";
import {
  companiesListAtom,
  editingCompanyAtom,
  selectedCompanyIdAtom,
} from "atoms";
import { levelsList } from "common";
import { FC, useState, useEffect } from "react";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  addCompany,
  deleteCompany,
  getCompanies,
  updateCompany,
} from "service";
import { Company } from "types";

export const CompanyForm: FC = () => {
  const setCompanies = useSetRecoilState(companiesListAtom);
  const [editedCompany, setEditedCompany] = useRecoilState(editingCompanyAtom);
  const setSelectedId = useSetRecoilState(selectedCompanyIdAtom);
  const [company, setCompany] = useState<Company | undefined>();

  useEffect(() => {
    setCompany(editedCompany);
  }, [editedCompany]);

  if (!company) return null;

  const isEditing = !!company.id;

  const handleClose = () => setEditedCompany(undefined);
  const handleDelete = () => {
    deleteCompany(company.id).then(() => {
      setCompanies((old) => old.filter((c) => c.id != company.id));
      setEditedCompany(undefined);
    });
  };

  const handleSave = () => {
    isEditing
      ? updateCompany(company).then(() => {
          setCompanies((old) => {
            return old.map((c) => (c.id === company.id ? company : c));
          });
        })
      : addCompany(company).then((id) => {
          getCompanies([id]).then((c) => setCompanies((old) => [...old, ...c]));
          setSelectedId(id);
        });

    setEditedCompany(undefined);
  };

  const allowSave = !!company.name;

  const title = !isEditing
    ? "Cоздание компании"
    : `Редактирование "${company.name}"`;

  return (
    <Dialog open={true} onClose={handleClose} maxWidth="xs">
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <TextField
          id="name"
          autoFocus
          size="small"
          margin="dense"
          label="Название"
          fullWidth
          variant="outlined"
          value={company.name || ""}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setCompany({ ...company, name: event.target.value });
          }}
        />
        <TextField
          id="comment"
          margin="dense"
          size="small"
          label="Комментарий"
          fullWidth
          multiline
          variant="outlined"
          minRows={2}
          value={company.comment || ""}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setCompany({ ...company, comment: event.target.value });
          }}
        />
        <FormControl fullWidth margin="dense" size="small">
          <InputLabel id="level-label">Уровень доверия</InputLabel>
          <Select
            labelId="level-label"
            id="level"
            size="small"
            label="Уровень доверия"
            fullWidth
            value={String(company.level)}
            onChange={(event: SelectChangeEvent) => {
              setCompany({
                ...company,
                level: +event.target.value as Company["level"],
              });
            }}
          >
            {levelsList.map((level, i) => (
              <MenuItem value={i} key={i}>
                {level}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        {isEditing ? (
          <TextField
            id="decisionMakerId"
            margin="dense"
            size="small"
            label="ЛПР"
            fullWidth
            disabled
            variant="outlined"
          />
        ) : null}
      </DialogContent>
      <DialogActions sx={{ mr: 2, ml: 2, mb: 2 }}>
        {isEditing && (
          <Button variant="text" color="error" onClick={handleDelete}>
            Удалить
          </Button>
        )}
        <Box sx={{ flex: "auto" }}></Box>
        <Button variant="text" onClick={handleClose}>
          Отмена
        </Button>
        <Button variant="contained" onClick={handleSave} disabled={!allowSave}>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
};
