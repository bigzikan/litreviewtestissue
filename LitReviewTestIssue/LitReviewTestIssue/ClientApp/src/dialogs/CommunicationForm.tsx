import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  TextField,
} from "@mui/material";
import {
  communicationsListAtom,
  // companiesListAtom,
  editingCommunicationAtom,
  contactsListAtom,
  selectedCompanyAtom,
} from "atoms";
import { commTypesList } from "common";
import { FC, useEffect, useState } from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import {
  addCommunication,
  deleteCommunication,
  getCommunicationsByCompany,
  updateCommunication,
} from "service";
import { Communication } from "types";

export const CommunicationForm: FC = () => {
  const [editedComm, setEditedComm] = useRecoilState(editingCommunicationAtom);
  // const companies = useRecoilValue(companiesListAtom);
  const company = useRecoilValue(selectedCompanyAtom);
  const contacts = useRecoilValue(contactsListAtom);
  const setCommunications = useSetRecoilState(communicationsListAtom);
  const [communication, setCommunication] = useState<
    Communication | undefined
  >();

  useEffect(() => {
    setCommunication(editedComm);
  }, [editedComm]);

  if (!communication || !company) return null;

  const isEditing = !!communication.id;

  const handleUpdateList = () => {
    getCommunicationsByCompany(company.id).then((c) => setCommunications(c));
  };

  const handleClose = () => setEditedComm(undefined);
  const handleDelete = () => {
    deleteCommunication(communication.id).then(handleUpdateList);
    setEditedComm(undefined);
  };

  const handleSave = () => {
    isEditing
      ? updateCommunication(communication).then(handleUpdateList)
      : addCommunication(communication).then(handleUpdateList);

    setEditedComm(undefined);
  };

  let allowSave = !!communication.contactId && !!communication.companyId;
  if (communication.type != 1 && !communication.phoneNumber) allowSave = false;
  if (communication.type != 0 && !communication.email) allowSave = false;

  const title = !isEditing
    ? "Добавление коммуникации"
    : `Редактирование коммуникации "${communication.contactId}"`;

  return (
    <Dialog open={true} onClose={handleClose} maxWidth="xs">
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <FormControl>
          <FormLabel id="type-label">Основной тип связи</FormLabel>
          <RadioGroup
            row
            aria-labelledby="type-label"
            defaultValue={2}
            value={communication.type}
            onChange={(_e, newValue) => {
              setCommunication({
                ...communication,
                type: +newValue as Communication["type"],
              });
            }}
          >
            {commTypesList.map((type, i) => (
              <FormControlLabel
                value={i}
                key={i}
                control={<Radio size="small" />}
                label={type}
              />
            ))}
          </RadioGroup>
        </FormControl>

        <TextField
          id="phoneNumber"
          margin="normal"
          size="small"
          label="Телефон"
          fullWidth
          required={communication.type != 1}
          variant="outlined"
          value={communication.phoneNumber}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setCommunication({
              ...communication,
              phoneNumber: event.target.value,
            });
          }}
        />
        <TextField
          id="email"
          margin="normal"
          size="small"
          label="Email"
          fullWidth
          required={communication.type != 0}
          variant="outlined"
          value={communication.email}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setCommunication({
              ...communication,
              email: event.target.value,
            });
          }}
        />

        <FormControl fullWidth margin="normal" size="small" required>
          <InputLabel id="contact-label">Контакт</InputLabel>
          <Select
            labelId="contact-label"
            id="contact"
            size="small"
            label="Контакт"
            fullWidth
            value={communication.contactId}
            onChange={(event: SelectChangeEvent) => {
              setCommunication({
                ...communication,
                contactId: event.target.value,
              });
            }}
          >
            {contacts.map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.fullName}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        {/* {isEditing ? (
          <FormControl fullWidth margin="normal" size="small" required>
            <InputLabel id="company-label">Компания</InputLabel>
            <Select
              labelId="company-label"
              id="company"
              size="small"
              label="Компания"
              fullWidth
              value={communication.companyId}
              onChange={(event: SelectChangeEvent) => {
                setCommunication({
                  ...communication,
                  companyId: event.target.value,
                });
              }}
            >
              {companies.map((company) => (
                <MenuItem key={company.id} value={company.id}>
                  {company.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        ) : null} */}
      </DialogContent>
      <DialogActions sx={{ mr: 2, ml: 2, mb: 2 }}>
        {isEditing && (
          <Button variant="text" color="error" onClick={handleDelete}>
            Удалить
          </Button>
        )}
        <Box sx={{ flex: "auto" }}></Box>
        <Button variant="text" onClick={handleClose}>
          Отмена
        </Button>
        <Button variant="contained" onClick={handleSave} disabled={!allowSave}>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
};
