export interface Company {
  id: string;
  name: string;
  level: 0 | 1 | 2 | 3;
  decisionMakerId?: string;
  comment?: string;
  creationTime: string;
  modificationTime: string;
}

export interface Contact {
  id: string;
  companyId: string;
  name: string;
  surname: string;
  middleName?: string;
  fullName: string;
  isDecisionMaker?: boolean;
  jobTitle: string;
  creationTime: string;
  modificationTime: string;
}

export interface Communication {
  id: string;
  companyId: string;
  contactId: string;
  type: 0 | 1 | 2;
  phoneNumber?: string;
  email?: string;
}

// type CommunicationWithRequiredField = Communication & (
// 	{ phoneNumber: string; email?: never } | { phoneNumber?: never; email: string }
//   );
