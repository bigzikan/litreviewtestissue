import * as ReactDOM from "react-dom/client";
import { App } from "./App";
import { RecoilRoot } from "recoil";

const el = document.getElementById("root");
if (el)
  ReactDOM.createRoot(el).render(
    <RecoilRoot>
      <App />
    </RecoilRoot>,
  );
