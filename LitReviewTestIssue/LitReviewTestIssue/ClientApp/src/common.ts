import { format } from "date-fns";
import data from "./appConfig.json";

export const baseApiUrl = `${data.schema}://localhost:${data.serverPort}/api`;

export const levelsList = ["Premium", "First", "Second", "Third"];
export const commTypesList = ["Телефон", "Email", "Все"];

export const formatDate = (date: string | number | Date) =>
  format(new Date(date), "dd.MM.yyyy");
