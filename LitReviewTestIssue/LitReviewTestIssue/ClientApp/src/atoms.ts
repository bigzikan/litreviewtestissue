import { Communication, Company, Contact } from "types";
import { atom, selector } from "recoil";

//companies
export const companiesListAtom = atom<Company[]>({
  key: "companiesList",
  default: [] as Company[],
});

export const selectedCompanyIdAtom = atom<string>({
  key: "selectedCompanyId",
  default: "",
});

export const selectedCompanyAtom = selector<Company | undefined>({
  key: "selectedCompany",
  get: ({ get }) => {
    const id = get(selectedCompanyIdAtom);
    return get(companiesListAtom).find((item) => item.id == id) || undefined;
  },
});

export const editingCompanyAtom = atom<Company | undefined>({
  key: "editedCompany",
  default: undefined,
});

//contacts
export const contactsListAtom = atom<Contact[]>({
  key: "contactsList",
  default: [] as Contact[],
});

export const editingContactAtom = atom<Contact | undefined>({
  key: "editingContact",
  default: undefined,
});

//communications
export const communicationsListAtom = atom<Communication[]>({
  key: "communicationsList",
  default: [] as Communication[],
});

export const editingCommunicationAtom = atom<Communication | undefined>({
  key: "editingCommunication",
  default: undefined,
});
