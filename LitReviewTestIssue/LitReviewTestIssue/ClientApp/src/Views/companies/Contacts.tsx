import { Box, Button, IconButton, TextField } from "@mui/material";
import { FC, useMemo, useState } from "react";
import { Contact } from "types";
import { AdvancedTable, AdvancedTableColumn } from "components";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import AddIcon from "@mui/icons-material/Add";
import { ContactsForm } from "dialogs/ContactsForm";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  editingContactAtom,
  contactsListAtom,
  selectedCompanyIdAtom,
} from "atoms";
import { formatDate } from "common";

export const Contacts: FC = () => {
  const selectedCompanyId = useRecoilValue(selectedCompanyIdAtom);
  const contactsList = useRecoilValue(contactsListAtom);
  const setEditingContact = useSetRecoilState(editingContactAtom);
  const [filter, setFilter] = useState("");

  const porsonColumns: AdvancedTableColumn<Contact>[] = [
    {
      id: "fullname",
      label: "ФИО",
      sx: { pl: 0 },
      sortBy: (user) => user.fullName,
      sortedDefault: true,
      cellContent: (user) => user.fullName,
    },
    {
      id: "job",
      label: "Должность",
      sortBy: (user) => user.jobTitle,
      cellContent: (user) => user.jobTitle,
    },
    {
      id: "decision-maker",
      label: "ЛПР",
      sortBy: (user) => user.isDecisionMaker,
      cellContent: (user) => (user.isDecisionMaker ? "Да" : ""),
    },
    {
      id: "added",
      label: "Дата добавления",
      sortBy: (user) => user.creationTime,
      cellContent: (user) => formatDate(user.creationTime),
    },
    {
      id: "modified",
      label: "Дата обновления",
      sortBy: (user) => user.modificationTime,
      cellContent: (user) => formatDate(user.modificationTime),
    },
    {
      id: "manage",
      label: "",
      sx: { width: "1px", whiteSpace: "nowrap", pr: 0 },
      cellContent: (user) => (
        <>
          <IconButton size="small" onClick={() => setEditingContact(user)}>
            <EditOutlinedIcon />
          </IconButton>
        </>
      ),
    },
  ];

  const filteredList = useMemo(() => {
    const f = filter.toLowerCase().trim();
    return f
      ? contactsList.filter(
          (c) =>
            c.fullName.toLowerCase().includes(f) ||
            c.jobTitle.toLowerCase().includes(f),
        )
      : contactsList;
  }, [filter, contactsList]);

  return (
    <Box>
      <TextField
        label="Фильтр по сотрудникам"
        variant="outlined"
        size="small"
        fullWidth
        sx={{ mb: 2 }}
        value={filter}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setFilter(event.target.value);
        }}
      />
      <AdvancedTable
        sortable
        pagination
        size="small"
        columns={porsonColumns}
        data={filteredList}
      />
      <Button
        sx={{ mt: -9 }}
        startIcon={<AddIcon />}
        onClick={() =>
          setEditingContact({
            id: "",
            companyId: selectedCompanyId,
            name: "Иван",
            middleName: "Иванович",
            surname: "Иванов",
            jobTitle: "",
            fullName: "",
            creationTime: "",
            modificationTime: "",
            isDecisionMaker: false,
          })
        }
      >
        Добавить
      </Button>

      <ContactsForm />
    </Box>
  );
};
