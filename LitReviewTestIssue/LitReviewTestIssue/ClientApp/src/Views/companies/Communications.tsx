import { Box, Button, IconButton, TextField } from "@mui/material";
import { FC, useMemo, useState } from "react";
import { Communication } from "types";
import { AdvancedTable, AdvancedTableColumn } from "components";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import AddIcon from "@mui/icons-material/Add";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  communicationsListAtom,
  editingCommunicationAtom,
  contactsListAtom,
  selectedCompanyIdAtom,
} from "atoms";
import { CommunicationForm } from "dialogs/CommunicationForm";
import { commTypesList } from "common";

export const Communications: FC = () => {
  const communications = useRecoilValue(communicationsListAtom);
  const setEditedCommunication = useSetRecoilState(editingCommunicationAtom);
  const selectedCompanyId = useRecoilValue(selectedCompanyIdAtom);
  const contacts = useRecoilValue(contactsListAtom);
  const [filter, setFilter] = useState("");

  const communicationsColumns: AdvancedTableColumn<Communication>[] = [
    {
      id: "contact",
      label: "Контакт",
      sx: { pl: 0 },
      sortBy: (item) => item.contactId,
      sortedDefault: true,
      cellContent: (item) =>
        contacts.find((c) => c.id == item.contactId)?.fullName,
    },
    {
      id: "type",
      label: "Основной тип связи",
      sortBy: (item) => item.type,
      cellContent: (item) => commTypesList[item.type],
    },
    {
      id: "phoneNumber",
      label: "Телефон",
      sortBy: (item) => item.phoneNumber,
      cellContent: (item) => item.phoneNumber,
    },
    {
      id: "email",
      label: "Email",
      sortBy: (item) => item.email,
      cellContent: (item) => item.email,
    },
    {
      id: "manage",
      label: "",
      sx: { width: "1px", whiteSpace: "nowrap", pr: 0 },
      cellContent: (item) => (
        <>
          <IconButton size="small" onClick={() => setEditedCommunication(item)}>
            <EditOutlinedIcon />
          </IconButton>
        </>
      ),
    },
  ];

  const filteredList = useMemo(() => {
    const f = filter.toLowerCase().trim();
    return f
      ? communications.filter(
          (comm) =>
            (comm.phoneNumber || "").toLowerCase().includes(f) ||
            (comm.email || "").toLowerCase().includes(f) ||
            (contacts.find((c) => c.id == comm.contactId)?.fullName || "")
              .toLowerCase()
              .includes(f),
        )
      : communications;
  }, [filter, communications]);

  return (
    <Box>
      <TextField
        label="Фильтр по коммуникациям"
        variant="outlined"
        size="small"
        fullWidth
        sx={{ mb: 2 }}
        value={filter}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setFilter(event.target.value);
        }}
      />
      <AdvancedTable
        sortable
        pagination
        size="small"
        columns={communicationsColumns}
        data={filteredList}
      />
      <Button
        sx={{ mt: -9 }}
        startIcon={<AddIcon />}
        onClick={() =>
          setEditedCommunication({
            id: "",
            companyId: selectedCompanyId,
            contactId: contacts.find((c) => c.isDecisionMaker)?.id || "",
            type: 2,
            phoneNumber: "",
            email: "",
          })
        }
      >
        Добавить
      </Button>

      <CommunicationForm />
    </Box>
  );
};
