import { Box, IconButton, Tab, Tabs, Typography } from "@mui/material";
import { FC, useState } from "react";
import { Contacts } from "./Contacts";
import { editingCompanyAtom, selectedCompanyAtom } from "atoms";
import { useRecoilValue, useSetRecoilState } from "recoil";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Communications } from "./Communications";
import { NoData } from "components";
import { formatDate, levelsList } from "common";

const DetailsItem: FC<{ name: string; value: string }> = ({ name, value }) => (
  <Typography>
    <Typography
      sx={{ display: "inline-block", width: 180 }}
      variant="body2"
      component="span"
    >
      {name}:
    </Typography>
    {value}
  </Typography>
);

export const CompanyMain: FC = () => {
  const selectedCompany = useRecoilValue(selectedCompanyAtom);
  const setEditedCompany = useSetRecoilState(editingCompanyAtom);

  const [selectedTab, setSelectedTab] = useState(0);

  const handleTabChange = (_event: React.SyntheticEvent, newValue: number) => {
    setSelectedTab(newValue);
  };

  if (!selectedCompany) return <NoData message="Компания не выбрана"></NoData>;

  return (
    <Box p={3} sx={{ flex: "auto" }}>
      <Typography variant="h4">
        {selectedCompany.name}
        <IconButton
          sx={{ ml: 2 }}
          onClick={() => {
            selectedCompany && setEditedCompany({ ...selectedCompany });
          }}
        >
          <EditOutlinedIcon />
        </IconButton>
      </Typography>

      <Typography>{selectedCompany.comment}</Typography>

      <Box mt={3}>
        <DetailsItem
          name="Создана"
          value={formatDate(selectedCompany.creationTime)}
        />
        <DetailsItem
          name="Обновлена"
          value={formatDate(selectedCompany.modificationTime)}
        />
        <DetailsItem
          name="Уровень доверия"
          value={
            selectedCompany.level ? levelsList[selectedCompany.level] : "-"
          }
        />
      </Box>

      <Box sx={{ mt: 3, mb: 3, borderBottom: 1, borderColor: "divider" }}>
        <Tabs value={selectedTab} onChange={handleTabChange}>
          <Tab
            sx={{ color: "WindowText", pl: 0, pr: 0, mr: 2 }}
            label="Сотрудники"
          />
          <Tab
            sx={{ color: "WindowText", pl: 0, pr: 0, mr: 2 }}
            label="Средства коммуникации"
          />
        </Tabs>
      </Box>

      {selectedTab == 0 ? <Contacts /> : null}
      {selectedTab == 1 ? <Communications /> : null}
    </Box>
  );
};
