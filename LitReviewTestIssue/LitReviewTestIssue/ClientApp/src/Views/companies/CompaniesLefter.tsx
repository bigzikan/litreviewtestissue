import {
  Box,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  TextField,
} from "@mui/material";
import {
  companiesListAtom,
  editingCompanyAtom,
  selectedCompanyIdAtom,
} from "atoms";
import AddIcon from "@mui/icons-material/Add";
import { FC, useMemo, useState } from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";

export const CompaniesLefter: FC = () => {
  const companies = useRecoilValue(companiesListAtom);
  const [selectedId, setSelectedId] = useRecoilState(selectedCompanyIdAtom);
  const setEditedCompany = useSetRecoilState(editingCompanyAtom);
  const [filter, setFilter] = useState("");

  const filteredList = useMemo(() => {
    const f = filter.toLowerCase().trim();
    return f
      ? companies.filter(
          (c) =>
            c.name.toLowerCase().includes(f) ||
            (c.comment || "").toLowerCase().includes(f),
        )
      : companies;
  }, [filter, companies]);

  return (
    <Box sx={{ flex: "none", width: 250, backgroundColor: "#f4f4f4" }}>
      <TextField
        label="Фильтр"
        variant="outlined"
        size="small"
        value={filter}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setFilter(event.target.value);
        }}
        sx={{ ml: 2, mt: 2, mr: 2, backgroundColor: "#fff" }}
      />
      <List>
        {filteredList.map((company) => {
          return (
            <ListItemButton
              selected={selectedId == company.id}
              key={company.id}
              onClick={() => setSelectedId(company.id)}
            >
              <ListItemText primary={company.name} />
            </ListItemButton>
          );
        })}
        {filteredList.length ? null : (
          <ListItemButton disabled>
            <ListItemText primary="Ничего не найдено" />
          </ListItemButton>
        )}
      </List>

      <ListItemButton
        onClick={() =>
          setEditedCompany({
            id: "",
            name: "Новая компания",
            level: 3,
            creationTime: "",
            modificationTime: "",
          })
        }
      >
        <ListItemIcon sx={{ minWidth: 32, color: "primary.main" }}>
          <AddIcon />
        </ListItemIcon>
        <ListItemText
          primary="Добавить компанию"
          sx={{ color: "primary.main" }}
        />
      </ListItemButton>
    </Box>
  );
};
