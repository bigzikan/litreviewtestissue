import { FC, useEffect } from "react";
import "./custom.css";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Box } from "@mui/material";
import { CompaniesLefter, CompanyMain } from "Views";
import { CompanyForm } from "dialogs/CompanyForm";
import {
  getCommunicationsByCompany,
  getCompanies,
  getContactsByCompany,
} from "service";
import {
  communicationsListAtom,
  companiesListAtom,
  contactsListAtom,
  selectedCompanyIdAtom,
} from "atoms";

export const App: FC = () => {
  const setCompanies = useSetRecoilState(companiesListAtom);
  const selectedCompanyId = useRecoilValue(selectedCompanyIdAtom);
  const setContacts = useSetRecoilState(contactsListAtom);
  const setCommunications = useSetRecoilState(communicationsListAtom);

  useEffect(() => {
    getCompanies().then(setCompanies);
  }, []);

  useEffect(() => {
    if (!selectedCompanyId) return;
    getContactsByCompany(selectedCompanyId).then(setContacts);
    getCommunicationsByCompany(selectedCompanyId).then(setCommunications);
  }, [selectedCompanyId]);

  return (
    <Box
      sx={{ display: "flex", width: "100%", height: "100%" }}
      component="main"
    >
      <CompaniesLefter />
      <CompanyMain />
      <CompanyForm />
    </Box>
  );
};
