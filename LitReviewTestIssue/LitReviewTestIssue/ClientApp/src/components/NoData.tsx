import { Box, Typography } from "@mui/material";
import { FC } from "react";

export const NoData: FC<{ message: string; icon?: string }> = ({ message }) => {
  return (
    <Box
      sx={{
        pt: 2,
        pb: 2,
        textAlign: "center",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Typography variant="body1">Hичего нет</Typography>
      <Typography variant="caption" sx={{ color: "text.secondary" }}>
        {message}
      </Typography>
    </Box>
  );
};
