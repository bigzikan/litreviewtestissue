// import { Link as RouterLink } from "react-router-dom";
import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";

export const Header: React.FC = () => {
  return (
    <AppBar position="static" sx={{ zIndex: 1 }}>
      <Toolbar variant="dense">
        {/* <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
		<MenuIcon />
		</IconButton> */}
        <Typography
          variant="h6"
          color="inherit"
          component="div"
          sx={{ flexGrow: 1 }}
        >
          LitReviewTestIssue
        </Typography>
        {/* <Button component={RouterLink} to="/" color="inherit">
          Companies
        </Button>
        <Button component={RouterLink} to="/contacts" color="inherit">
          Contacts
        </Button> */}
      </Toolbar>
    </AppBar>
  );
};
