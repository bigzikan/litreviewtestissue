import React from "react";
import { Header } from "./Header";
import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";

export const Layout: React.FC = () => {
  return (
    <>
      <Header />
      <Box sx={{ flex: "auto" }}>
        <Outlet />
      </Box>
    </>
  );
};
