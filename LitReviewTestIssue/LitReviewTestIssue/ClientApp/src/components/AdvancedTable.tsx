import {
  Box,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from "@mui/material";
import { NoData } from "./NoData";
import { cloneDeep, sortBy } from "lodash";
import React, { useMemo, useState } from "react";

export interface AdvancedTableColumn<Item> {
  id: string;
  label: string;
  align?: "right" | "left" | "center";
  sx?: SxProps;
  sortBy?:
    | string
    | ((item: Item) => string | number | boolean | null | undefined);
  cellContent?: (item: Item) => React.ReactNode | null | undefined;
  sortedDefault?: boolean;
}

export interface AdvancedTableProps<Item> {
  columns: AdvancedTableColumn<Item>[];
  data: Item[];
  sx?: SxProps;
  size?: "small" | "medium";
  ariaLabel?: string;
  sortable?: boolean;
  pagination?: boolean;
  sortByDefault?: string;
  rowsPerPageOptions?: Array<number | { label: string; value: number }>;
  defaultSortDirection?: "asc" | "desc";
  noDataMessage?: string;
}

export function AdvancedTable<Item>({
  data,
  columns,
  sx,
  ariaLabel,
  size,
  sortable,
  pagination,
  rowsPerPageOptions = [5, 10, 25],
  defaultSortDirection = "desc",
  noDataMessage = "Попробуйте добавить элементы или изменить фильтр",
}: AdvancedTableProps<Item>) {
  //
  const defaultSortKey =
    columns.filter((col) => col.sortedDefault)[0]?.id || "";

  const [sortDirection, setSortDirection] = useState<
    "asc" | "desc" | undefined
  >(defaultSortDirection);
  const [sortedBy, setSortedBy] = useState(defaultSortKey);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleSort = (id: string) => {
    if (sortedBy === id) {
      setSortDirection((oldOrder) => (oldOrder === "asc" ? "desc" : "asc"));
    } else setSortedBy(id);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const sortedList = useMemo(() => {
    const sortByProp =
      (sortedBy && columns.find((col) => col.id === sortedBy)?.sortBy) || "";
    const sortFunc =
      sortByProp === "string"
        ? (item: Item) => item[sortByProp as keyof Item]
        : sortByProp;
    let newList = sortFunc ? sortBy(cloneDeep(data), sortFunc) : data;
    if (sortDirection === "desc") newList.reverse();
    if (pagination)
      newList = newList.slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage,
      );
    return newList;
  }, [columns, data, sortDirection, sortedBy, page, rowsPerPage, pagination]);

  return (
    <TableContainer>
      <Table sx={sx} aria-label={ariaLabel} size={size}>
        <TableHead>
          <TableRow>
            {columns.map((column) => (
              <TableCell
                key={column.id}
                align={column.align || "left"}
                sx={column.sx}
                sortDirection={sortedBy === column.id ? sortDirection : false}
              >
                {sortable && column.sortBy ? (
                  <TableSortLabel
                    active={sortedBy === column.id}
                    direction={sortDirection}
                    onClick={() => handleSort(column.id)}
                  >
                    {column.label}
                    {sortedBy === column.id ? (
                      <Box component="span" sx={{ display: "none" }}>
                        {sortDirection === "desc"
                          ? "sorted descending"
                          : "sorted ascending"}
                      </Box>
                    ) : null}
                  </TableSortLabel>
                ) : (
                  column.label
                )}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {sortedList.length ? (
            sortedList.map((item: Item, rowIndex: number) => (
              <TableRow key={rowIndex}>
                {columns.map((column) => (
                  <TableCell
                    align={column.align || "left"}
                    key={column.id}
                    sx={column.sx}
                  >
                    {column.cellContent ? column.cellContent(item) : null}
                  </TableCell>
                ))}
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={columns.length}>
                <NoData message={noDataMessage} />
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
      {pagination ? (
        <TablePagination
          rowsPerPageOptions={rowsPerPageOptions}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={(_event, newPage: number) => {
            setPage(newPage);
          }}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      ) : null}
    </TableContainer>
  );
}
