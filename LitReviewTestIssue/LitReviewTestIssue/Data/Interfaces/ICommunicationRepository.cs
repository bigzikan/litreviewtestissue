﻿using LitReviewTestIssue.Data.Models;

namespace LitReviewTestIssue.Data.Interfaces
{
    public interface ICommunicationRepository
    {
        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateСommunication(Communication model);

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<Communication>> GetСommunicationsInfo(Communication model);

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteСommunication(Guid id);

        /// <summary>
        /// Получить инфо о коммуникации по Id
        /// </summary>
        /// <returns></returns>
        Task<List<Communication>> GetCommunicationsById(List<Guid>? listIds);

        /// <summary>
        /// Получить список коммуникаций по Id контакта
        /// </summary>
        /// <returns></returns>
        Task<List<Communication>> GetCommunicationsByContactId(Guid contactId);

        /// <summary>
        /// Получить список коммуникаций по Id компании
        /// </summary>
        /// <returns></returns>
        Task<List<Communication>> GetCommunicationsByCompanyId(Guid companyId);

        /// <summary>
        /// Редактировать коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> UpdateCommunication(Communication model);
    }
}
