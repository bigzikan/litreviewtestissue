﻿using LitReviewTestIssue.Data.Models;

namespace LitReviewTestIssue.Data.Interfaces
{
    public interface IContactService
    {
        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateContact(Contact model);

        /// <summary>
        /// Получить список сотрудников по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<Contact>> GetContactsInfo(Contact model);

        /// <summary>
        /// Редактировать инфо о сотруднике
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> UpdateContact(Contact model);

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteContact(Guid id);

        /// <summary>
        /// Получить инфо о сотруднике по Id
        /// </summary>
        /// <returns></returns>
        Task<List<Contact>> GetContactsById(List<Guid>? listIds);

        /// <summary>
        /// Получить список контактов по Id компании
        /// </summary>
        /// <returns></returns>
        Task<List<Contact>> GetContactsByCompanyId(Guid Id);
    }
}
