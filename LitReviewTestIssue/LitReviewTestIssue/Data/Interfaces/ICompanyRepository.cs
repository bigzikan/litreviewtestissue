﻿using LitReviewTestIssue.Data.Models;

namespace LitReviewTestIssue.Data.Interfaces
{
    public interface ICompanyRepository
    {
        /// <summary>
        /// Добавить компанию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateCompany(Company model);

        /// <summary>
        /// Получить список компаний по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<Company>> GetCompaniesInfo(Company model);

        /// <summary>
        /// Редактировать инфо о компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> UpdateCompany(Company model);

        /// <summary>
        /// Удалить компанию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteCompany(Guid id);

        /// <summary>
        /// Получить инфо о компаниях по Id
        /// </summary>
        /// <returns></returns>
        Task<List<Company>> GetCompaniesById(List<Guid>? listIds);
    }
}
