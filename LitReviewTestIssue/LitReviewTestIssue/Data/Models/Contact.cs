﻿namespace LitReviewTestIssue.Data.Models
{
    public class Contact
    {
        private string name = string.Empty;
        private string surName = string.Empty;
        private string middleName = string.Empty;

        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string? Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string? Surname
        {
            get
            {
                return surName;
            }
            set
            {
                surName = value;
            }
        }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? MiddleName
        {
            get
            {
                return middleName;
            }
            set
            {
                middleName = value;
            }
        }

        /// <summary>
        /// ФИО
        /// </summary>
        public string? FullName
        {
            get
            {
                return $"{surName} {name} {middleName}";
            }
            set
            {
            }
        }

        /// <summary>
        /// Id компании
        /// </summary>
        public Guid CompanyId { get; set; }


        /// <summary>
        /// ЛПР
        /// </summary>
        public bool IsDecisionMaker { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? ModificationTime { get; set; }
    }
}
