﻿using LitReviewTestIssue.Enums;
using System.ComponentModel.DataAnnotations;

namespace LitReviewTestIssue.Data.Models
{
    public class Company
    {
        /// <summary>
        /// Id компании
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Уровень доверия
        /// </summary>
        public LevelsEnum Level { get; set; }

        /// <summary>
        /// ЛПР
        /// </summary>
        public Guid DecisionMakerId { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>        
        public string? Comment { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? ModificationTime { get; set; }
    }
}
