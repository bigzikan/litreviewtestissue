﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace LitReviewTestIssue.Data.Repository
{
    public class CommunicationRepository : ICommunicationRepository
    {
        private readonly ServerDBContext _appContext;

        public CommunicationRepository(ServerDBContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<Guid> CreateСommunication(Communication model)
        {
            await _appContext.Communications.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }

        public async Task<bool> DeleteСommunication(Guid id)
        {
            var communication = await _appContext.Communications.FirstOrDefaultAsync(p => p.Id == id);

            _appContext.Communications.Remove(communication);
            _appContext.SaveChanges();

            return true;
        }

        public async Task<List<Communication>> GetСommunicationsInfo(Communication model)
        {
            return await _appContext.Communications
                .Select(x => x)
                .Where(p => p.PhoneNumber == model.PhoneNumber ||
                            p.CompanyId == model.CompanyId ||
                            p.ContactId == model.ContactId ||
                            p.Type == model.Type)
                .ToListAsync();
        }

        /// <summary>
        /// Получить инфо о коммуникациях по Id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsById(List<Guid>? listIds)
        {
            if (listIds == null || listIds.Count == 0)
                return await _appContext.Communications.ToListAsync();
            else
                return await _appContext.Communications.Where(p => listIds.Contains(p.Id)).ToListAsync();
        }

        /// <summary>
        /// Получить список коммуникаций по Id контакта
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsByContactId(Guid contactId)
        {
            return await _appContext.Communications.Where(p => p.ContactId == contactId).ToListAsync();
        }

        /// <summary>
        /// Получить список коммуникаций по Id компании
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsByCompanyId(Guid companyId)
        {
            return await _appContext.Communications.Where(p => p.CompanyId == companyId).ToListAsync();
        }

        /// <summary>
        /// Редактировать коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCommunication(Communication model)
        {
            _appContext.Entry(model).State = EntityState.Modified;
            await Task.Run(() => _appContext.Communications.Update(model));
            _appContext.SaveChanges();
            return true;
        }
    }
}
