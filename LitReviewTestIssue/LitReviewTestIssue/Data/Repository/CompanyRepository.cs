﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace LitReviewTestIssue.Data.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ServerDBContext _appContext;

        public CompanyRepository(ServerDBContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<Guid> CreateCompany(Company model)
        {
            await _appContext.Companies.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }

        public async Task<bool> DeleteCompany(Guid id)
        {
            var company = await _appContext.Companies.FirstOrDefaultAsync(p => p.Id == id);

            _appContext.Companies.Remove(company);
            _appContext.SaveChanges();

            return true;
        }

        public async Task<List<Company>> GetCompaniesInfo(Company model)
        {
            return await _appContext.Companies
                .Select(x => x)
                .Where(p => p.CreationTime == model.CreationTime ||
                            p.DecisionMakerId == model.DecisionMakerId)
                .ToListAsync();
        }

        public async Task<bool> UpdateCompany(Company model)
        {
            Company company = await _appContext.Companies.FirstOrDefaultAsync(p => p.Id == model.Id);
            _appContext.Entry(company).State = EntityState.Modified;
            await Task.Run(() => _appContext.Companies.Update(company));
            _appContext.SaveChanges();
            return true;
        }

        public async Task<List<Company>> GetCompaniesById(List<Guid>? listIds)
        {
            if (listIds == null || listIds.Count == 0)
                return await _appContext.Companies.ToListAsync();
            else
                return await _appContext.Companies.Where(p => listIds.Contains(p.Id)).ToListAsync();
        }
    }
}
