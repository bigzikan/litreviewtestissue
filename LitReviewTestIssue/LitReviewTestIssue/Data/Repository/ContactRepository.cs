﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace LitReviewTestIssue.Data.Repository
{
    public class ContactRepository : IContactRepository
    {
        private readonly ServerDBContext _appContext;

        public ContactRepository(ServerDBContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<Guid> CreateContact(Contact model)
        {
            await _appContext.Contacts.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }

        public async Task<bool> DeleteContact(Guid id)
        {
            var contact = await _appContext.Contacts.FirstOrDefaultAsync(p => p.Id == id);
            _appContext.Contacts.Remove(contact);
            _appContext.SaveChanges();

            return true;
        }

        public async Task<List<Contact>> GetContactsInfo(Contact model)
        {
            return await _appContext.Contacts
                .Select(x => x)
                .Where(p => p.Name == model.Name ||
                            p.Surname == model.Surname ||
                            p.CreationTime == model.CreationTime ||
                            p.ModificationTime == model.ModificationTime)
                .ToListAsync();
        }

        public async Task<bool> UpdateContact(Contact model)
        {
            _appContext.Entry(model).State = EntityState.Modified;
            await Task.Run(() => _appContext.Contacts.Update(model));
            _appContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// Получить инфо о сотрудниках по Id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Contact>> GetContactsById(List<Guid>? listIds)
        {
            if (listIds == null || listIds.Count == 0)
                return await _appContext.Contacts.ToListAsync();
            else
                return await _appContext.Contacts.Where(p => listIds.Contains(p.Id)).ToListAsync();
        }

        public async Task<List<Contact>> GetContactsByCompanyId(Guid CompanyId)
        {
            return await _appContext.Contacts
                .Select(x => x)
                .Where(p => p.CompanyId == CompanyId)
                .ToListAsync();
        }
    }
}
