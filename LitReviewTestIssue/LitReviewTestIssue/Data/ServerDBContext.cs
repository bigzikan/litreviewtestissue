﻿using LitReviewTestIssue.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Transactions;

namespace LitReviewTestIssue.Data
{
    /// <summary>
    /// БД Контекст сервера
    /// </summary>
    public class ServerDBContext : DbContext
    {
        /// <summary>
        /// Конструктор контекста
        /// </summary>
        /// <param name="options"></param>
        public ServerDBContext(DbContextOptions<ServerDBContext> options) : base(options)
        { 
        }

        /// <summary>
        /// Таблица компаний
        /// </summary>
        public DbSet<Company> Companies { get; set; }

        /// <summary>
        /// Таблица контактов
        /// </summary>
        public DbSet<Contact> Contacts { get; set; }

        /// <summary>
        /// Таблица коммуникаций
        /// </summary>
        public DbSet<Communication> Communications { get; set; }
    }
}
