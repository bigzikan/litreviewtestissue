﻿namespace LitReviewTestIssue.Enums
{
    /// <summary>
    /// Уровень доверия
    /// </summary>
    public enum LevelsEnum
    {
        Premium = 0,
        First = 1,
        Second = 2,
        Third = 3
    };
}
