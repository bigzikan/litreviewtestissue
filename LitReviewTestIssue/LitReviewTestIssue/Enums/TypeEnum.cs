﻿namespace LitReviewTestIssue.Enums
{
    /// <summary>
    /// Тип связи
    /// </summary>
    public enum TypeEnum
    {
        Phone = 0,
        Email = 1,
        All = 2
    }
}
