using LitReviewTestIssue.Data;
using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Repository;
using LitReviewTestIssue.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();
builder.Services.AddScoped<IContactRepository, ContactRepository>();
builder.Services.AddScoped<ICommunicationRepository, CommunicationRepository>();

builder.Services.AddScoped<ICompanyService, CompanyService>();
builder.Services.AddScoped<IContactService, ContactService>();
builder.Services.AddScoped<ICommunicationService, CommunicationService>();

builder.Services.AddControllersWithViews();

builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "LitReviewTestIssue",
        Version = "v1",
        Description = "�������� ������� ��� ������� LitReview"
    });

    var xmlFile = $"{typeof(Program).Assembly.GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    s.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
});

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddHealthChecks();

builder.Services.AddEntityFrameworkNpgsql().AddDbContext<ServerDBContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection String 'LitReviewTestIssue' not found."));
});

builder.Services.AddCors();

Microsoft.Extensions.Configuration.ConfigurationManager configuration = builder.Configuration;

var app = builder.Build();

app.UseCors(builder => builder.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .SetIsOriginAllowed(origin => true));

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        string swaggerJsonBasePath = string.IsNullOrWhiteSpace(options.RoutePrefix) ? "." : "..";
        options.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "API");
        options.EnableFilter();
        options.DisplayRequestDuration();
    });
//}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseAuthorization();

app.UsePathBase(new PathString(configuration["AppName"]));

app.MapControllers();

app.MapFallbackToFile("index.html");

app.MapHealthChecks("/healthz");

app.Run();
