﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Data.Repository;
using System;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace LitReviewTestIssue.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _company;
        private readonly IContactRepository _contact;
        private readonly ICommunicationRepository _communication;

        public CompanyService(ICompanyRepository company, IContactRepository contact, ICommunicationRepository communication)
        {
            _company = company;
            _contact = contact;
            _communication = communication;
        }

        /// <summary>
        /// Добавить компанию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateCompany(Company model)
        {
            model.CreationTime = DateTime.Now;
            return await _company.CreateCompany(model);
        }

        /// <summary>
        /// Получить список компаний по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Company>> GetCompaniesInfo(Company model)
        {
            return await _company.GetCompaniesInfo(model);
        }

        /// <summary>
        /// Редактировать инфо о компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCompany(Company model)
        {
            var company = (await _company.GetCompaniesById(new List<Guid> { model.Id }))[0];

            if (company == null)
                throw new Exception($"Компании с id {model.Id} не существует!");

            model.ModificationTime = DateTime.Now;

            return await _company.UpdateCompany(model);
        }

        /// <summary>
        /// Удалить компанию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCompany(Guid id)
        {
            var company = (await _company.GetCompaniesById(new List<Guid> { id }))[0];

            if (company == null)
                throw new Exception($"Компании с id - {company.Id} не существует!");

            var communications = await _communication.GetCommunicationsByCompanyId(id);
            foreach (var communication in communications)
            {
                await _communication.DeleteСommunication(communication.Id);
            }

            var contacts = await _contact.GetContactsByCompanyId(id);
            foreach (var contact in contacts)
            {
                await _contact.DeleteContact(contact.Id);
            }

            return await _company.DeleteCompany(id);
        }

        /// <summary>
        /// Получить инфо о компаниях по Id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Company>> GetCompaniesById(List<Guid>? listIds)
        {
            return await _company.GetCompaniesById(listIds);
        }
    }
}
