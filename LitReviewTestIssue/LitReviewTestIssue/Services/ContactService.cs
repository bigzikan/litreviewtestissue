﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using System;
using System.Diagnostics.Contracts;
using System.Reflection.Metadata;
using System.Reflection.Metadata.Ecma335;

namespace LitReviewTestIssue.Services
{
    public class ContactService : IContactService
    {
        private IContactRepository _contact;
        private ICommunicationService _communication;
        private ICompanyService _company;

        public ContactService(IContactRepository contact, ICommunicationService communication, ICompanyService company)
        {
            _contact = contact;
            _communication = communication;
            _company = company;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateContact(Contact model)
        {
            model.CreationTime = DateTime.Now;
            var id = await _contact.CreateContact(model);
            model.Id = id;

            if (model.IsDecisionMaker)
            {
                await SetDecisionMakerInCompany(model);
            }

            return model.Id;
        }

        /// <summary>
        /// Получить список сотрудников по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Contact>> GetContactsInfo(Contact model)
        {
            return await _contact.GetContactsInfo(model);
        }

        /// <summary>
        /// Редактировать инфо о сотруднике
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateContact(Contact model)
        {
            var contact = (await _contact.GetContactsById(new List<Guid> { model.Id }))[0];
            if (contact == null)
            {
                throw new Exception("Не существует контакта с указанным Id!");
            }

            model.ModificationTime = DateTime.Now;
            if (model.IsDecisionMaker)
            {
                await SetDecisionMakerInCompany(model);
            }

            return await _contact.UpdateContact(model);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteContact(Guid id)
        {
            var contact = (await _contact.GetContactsById(new List<Guid> { id }))[0];

            if (contact == null)
                throw new Exception($"Контакта с id - {contact.Id} не существует!");

            var communications = await _communication.GetCommunicationsByContactId(contact.Id);
            foreach (var communication in communications) 
            {
               await _communication.DeleteСommunication(communication.Id);               
            }

            return await _contact.DeleteContact(id);
        }

        /// <summary>
        /// Получить инфо о сотрудниках по Id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Contact>> GetContactsById(List<Guid>? listIds)
        { 
            return await _contact.GetContactsById(listIds);
        }

        /// <summary>
        /// Получить список контактов по Id компании
        /// </summary>
        /// <returns></returns>
        public async Task<List<Contact>> GetContactsByCompanyId(Guid Id)
        {
            return await _contact.GetContactsByCompanyId(Id);
        }

        /// <summary>
        /// Задать ЛПР в компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task SetDecisionMakerInCompany(Contact model)
        {
            var company = (await _company.GetCompaniesById(new List<Guid> { model.Id }))[0];

            if (company == null)
                throw new Exception("Невозможно назначить ЛПР данный контакт! Необходимо сначала назначить контакту компанию.");

            company.DecisionMakerId = model.Id;
            await _company.UpdateCompany(company);

            // Находим прочих сотрудников в компании и проставляем у них IsDecisionMaker в false
            var employees = await _contact.GetContactsByCompanyId(company.Id);

            employees = employees
                .Where(p => p.Id != model.Id)
                .Select(x => x)
                .ToList();

            employees.ForEach(a => a.IsDecisionMaker = false);

            foreach (var employee in employees)
            {
                if (employee == null)
                    continue;

                await _contact.UpdateContact(employee);
            }

            // Находим коммуникации компании с пустым ContactID и проставляем у них guid ЛПР            
            var companyCommunications = await _communication.GetCommunicationsByCompanyId(company.Id);

            companyCommunications
                .Where(p => p.ContactId == Guid.Empty)
                .ToList()
                .ForEach(a => a.ContactId = model.Id);

            foreach (var companyCommunication in companyCommunications)
            {
                if (companyCommunication == null)
                    continue;

                await _communication.UpdateCommunication(companyCommunication);
            }
        }
    }
}
