﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Data.Repository;
using System;
using System.ComponentModel.Design;

namespace LitReviewTestIssue.Services
{
    public class CommunicationService : ICommunicationService
    {
        private ICommunicationRepository _communication;

        public CommunicationService(ICommunicationRepository communication)
        {
            _communication = communication;
        }

        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateСommunication(Communication model)
        {
            return await _communication.CreateСommunication(model);
        }

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Communication>> GetСommunicationsInfo(Communication model)
        {
            return await _communication.GetСommunicationsInfo(model);
        }

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteСommunication(Guid id)
        {
            var communications = await _communication.GetCommunicationsById(new List<Guid> { id });

            if (communications == null)
            {
                throw new Exception("Коммуникации с таким Id не существует!");
            }

            return await _communication.DeleteСommunication(id);
        }

        /// <summary>
        /// Получить инфо о коммуникациях по Id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsById(List<Guid>? listIds)
        {
            return await _communication.GetCommunicationsById(listIds);
        }

        /// <summary>
        /// Получить список коммуникаций по Id контакта
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsByContactId(Guid contactId)
        {
            return await _communication.GetCommunicationsByContactId(contactId);
        }

        /// <summary>
        /// Получить список коммуникаций по Id компании
        /// </summary>
        /// <returns></returns>
        public async Task<List<Communication>> GetCommunicationsByCompanyId(Guid companyId)
        {
            return await _communication.GetCommunicationsByCompanyId(companyId);
        }

        /// <summary>
        /// Редактировать коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCommunication(Communication model)
        {
            var communication = (await _communication.GetCommunicationsById(new List<Guid> { model.Id }))[0];
            if (communication == null)
                return false;

            return await _communication.UpdateCommunication(model);
        }
    }
}
