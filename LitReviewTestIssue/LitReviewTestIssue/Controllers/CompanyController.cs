﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LitReviewTestIssue.Controllers
{
    /// <summary>
    /// Контроллер с запросами Company
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private ICompanyService _companyService;

        /// <summary>
        /// Контроллер с запросами Company
        /// </summary>
        public CompanyController(ICompanyService company)
        {
            _companyService = company;
        }

        /// <summary>
        /// Добавить компанию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateCompany([FromBody] Company model)
        {
            if (model.Id != Guid.Empty)
                throw new Exception("Данная компания уже существует!");

            var result = await _companyService.CreateCompany(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список компаний по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filter")]
        public async Task<IActionResult> GetCompaniesInfo([FromBody] Company model)
        {
            var result = await _companyService.GetCompaniesInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Редактировать инфо о компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateCompany([FromBody] Company model)
        {
            if (model.Id == Guid.Empty)
                throw new Exception("Необходимо указать Id записи!");

            var result = await _companyService.UpdateCompany(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить компанию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeleteCompany(Guid id)
        {
            var result = await _companyService.DeleteCompany(id);
            return Ok(result);
        }

        /// <summary>
        /// Получить инфо о компаниях по Id. Если Id не указаны, вернётся весь список
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getCompaniesById")]
        public async Task<IActionResult> GetCompaniesById([FromBody] List<Guid>? listIds)
        {
            var result = await _companyService.GetCompaniesById(listIds);
            return Ok(result);
        }
    }
}
