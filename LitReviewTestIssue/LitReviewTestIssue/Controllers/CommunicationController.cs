﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LitReviewTestIssue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunicationController : ControllerBase
    {
        private ICommunicationService _communicationService;

        public CommunicationController(ICommunicationService communication)
        {
            _communicationService = communication;
        }

        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateСommunication([FromBody] Communication model)
        {
            if (model.Id != Guid.Empty)
                throw new Exception("Данная запись уже существует!");

            var result = await _communicationService.CreateСommunication(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filter")]
        public async Task<IActionResult> GetСommunicationsInfo([FromBody] Communication model)
        {
            var result = await _communicationService.GetСommunicationsInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeleteСommunication(Guid id)
        {
            var result = await _communicationService.DeleteСommunication(id);
            return Ok(result);
        }

        /// <summary>
        /// Получить инфо о коммуникациях по Id. Если Id не указаны, вернётся весь список
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getCommunicationsById")]
        public async Task<IActionResult> GetCommunicationById([FromBody] List<Guid>? listIds)
        {
            var result = await _communicationService.GetCommunicationsById(listIds);
            return Ok(result);
        }

        /// <summary>
        /// Получить список коммуникаций по Id контакта
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getCommunicationByContactsId/{id}")]
        public async Task<IActionResult> GetCommunicationByContactsId(Guid Id)
        {
            var result = await _communicationService.GetCommunicationsByContactId(Id);
            return Ok(result);
        }

        /// <summary>
        /// Получить список коммуникаций по Id компании
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCommunicationsByCompanyId/{companyId}")]
        public async Task<List<Communication>> GetCommunicationsByCompanyId(Guid companyId)
        {
            return await _communicationService.GetCommunicationsByCompanyId(companyId);
        }

        /// <summary>
        /// Редактировать коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateCommunication([FromBody] Communication model)
        {
            if (model.Id == Guid.Empty)
                throw new Exception("Необходимо указать Id записи!");

            var result = await _communicationService.UpdateCommunication(model);
            return Ok(result);
        }
    }
}
