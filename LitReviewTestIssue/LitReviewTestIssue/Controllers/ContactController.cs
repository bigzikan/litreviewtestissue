﻿using LitReviewTestIssue.Data.Interfaces;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LitReviewTestIssue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private IContactService _contactService;

        public ContactController(IContactService contact)
        {
            _contactService = contact;
        }

        /// <summary>
        /// Добавить контакт
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateContact([FromBody] Contact model)
        {
            if (model.Id != Guid.Empty)
                throw new Exception("Данный контакт уже существует!");

            var result = await _contactService.CreateContact(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список контактов по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("filter")]
        public async Task<IActionResult> GetContactsInfo([FromBody] Contact model)
        {
            var result = await _contactService.GetContactsInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Редактировать инфо о контакте
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> UpdateContact([FromBody] Contact model)
        {
            if (model.Id == Guid.Empty)
                throw new Exception("Необходимо указать Id записи!");

            var result = await _contactService.UpdateContact(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить контакт
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeleteContact(Guid id)
        {
            var result = await _contactService.DeleteContact(id);
            return Ok(result);
        }

        /// <summary>
        /// Получить инфо о сотрудниках по Id. Если Id не указаны, вернётся весь список
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getContactsById")]
        public async Task<IActionResult> GetContactsById(List<Guid>? listIds)
        {
            var result = await _contactService.GetContactsById(listIds);
            return Ok(result);
        }

        /// <summary>
        /// Получить список контактов по Id компании
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getContactsCompanyById/{id}")]
        public async Task<IActionResult> GetContactsCompanyById(Guid Id)
        {
            var result = await _contactService.GetContactsByCompanyId(Id);
            return Ok(result);
        }
    }
}
