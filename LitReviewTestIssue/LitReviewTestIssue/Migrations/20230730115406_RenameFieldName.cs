﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LitReviewTestIssue.Migrations
{
    /// <inheritdoc />
    public partial class RenameFieldName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Сommunications",
                table: "Сommunications");

            migrationBuilder.RenameTable(
                name: "Сommunications",
                newName: "Communications");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Companies",
                newName: "Level");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Communications",
                table: "Communications",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Communications",
                table: "Communications");

            migrationBuilder.RenameTable(
                name: "Communications",
                newName: "Сommunications");

            migrationBuilder.RenameColumn(
                name: "Level",
                table: "Companies",
                newName: "Description");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Сommunications",
                table: "Сommunications",
                column: "Id");
        }
    }
}
