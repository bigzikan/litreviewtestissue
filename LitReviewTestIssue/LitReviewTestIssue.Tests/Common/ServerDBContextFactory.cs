﻿using LitReviewTestIssue.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LitReviewTestIssue.Tests.Common
{
    public class ServerDBContextFactory
    {
        public static Guid CompanyId = Guid.NewGuid();

        public static Guid ContactId = Guid.NewGuid();

        public static Guid CommunicationId = Guid.NewGuid();

        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
            return config;
        }

        public static ServerDBContext Create()
        {
            var config = InitConfiguration();
            var options = new DbContextOptionsBuilder<ServerDBContext>()
                            .UseNpgsql(config.GetConnectionString("TestConnection"))                            
                            .Options;

            var context = new ServerDBContext(options);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);                        
            context.Database.EnsureCreated();            
            //context.Database.Migrate();
            return context;
        }

        public static void Destroy(ServerDBContext context)
        {
            //context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
