﻿using LitReviewTestIssue.Data;
using LitReviewTestIssue.Data.Repository;
using LitReviewTestIssue.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LitReviewTestIssue.Tests.Common
{
    public abstract class TestCommandBase : IDisposable
    {
        protected readonly ServerDBContext context;

        protected readonly CompanyRepository companyRepository;
        protected readonly ContactRepository contactRepository;
        protected readonly CommunicationRepository communicationRepository;

        protected readonly CompanyService сompanyService;
        protected readonly ContactService contactService;
        protected readonly CommunicationService communicationService;

        public TestCommandBase() 
        {
            context = ServerDBContextFactory.Create();

            companyRepository = new CompanyRepository(context);
            contactRepository = new ContactRepository(context);
            communicationRepository = new CommunicationRepository(context);

            сompanyService = new CompanyService(companyRepository, contactRepository, communicationRepository);            
            communicationService = new CommunicationService(communicationRepository);
            contactService = new ContactService(contactRepository, communicationService, сompanyService);
        }

        public void Dispose()
        {
            ServerDBContextFactory.Destroy(context);
        }
    }
}
