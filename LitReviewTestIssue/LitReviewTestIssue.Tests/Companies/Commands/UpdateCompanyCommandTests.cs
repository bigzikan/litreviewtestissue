﻿using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Tests.Common;
using Xunit;

namespace LitReviewTestIssue.Tests.Companies.Commands
{
    public class UpdateCompanyCommandTests : TestCommandBase
    {
        [Fact]
        public async Task UpdateCompanyCommandTests_Success()
        {
            // Arrange
            var name = "test";
            var comment = "test";
            var creationTime = DateTime.Now;
            var decisionMakerId = Guid.Empty;
            var level = Enums.LevelsEnum.Premium;

            var company = new Company
            {
                Name = name,
                Comment = comment,
                CreationTime = creationTime,
                DecisionMakerId = decisionMakerId,
                Level = level
            };

            // Act
            var companyId = await сompanyService.CreateCompany(company);
            company.Id = companyId;
            company.ModificationTime = creationTime;            

            // Assert
            Assert.True(await сompanyService.UpdateCompany(company));
        }

        [Fact]
        public async Task UpdateContactCommandTests_FailOnWrongId()
        {
            // Arrange            
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;

            var contact = new Contact
            {
                Name = name,
                Surname = name,
                MiddleName = name,
                CompanyId = ServerDBContextFactory.CompanyId,
                CreationTime = creationTime,
                IsDecisionMaker = false,
                JobTitle = jobTitle
            };

            // Act            
            await contactService.CreateContact(contact);
            contact.Id = ServerDBContextFactory.CompanyId;

            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await contactService.UpdateContact(contact));
        }
    }
}
