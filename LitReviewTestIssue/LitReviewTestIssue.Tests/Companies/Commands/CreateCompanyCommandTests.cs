﻿using LitReviewTestIssue.Tests.Common;
using Xunit;
using LitReviewTestIssue.Data.Models;

namespace LitReviewTestIssue.Tests.Companies.Commands
{
    public class CreateCompanyCommandTests : TestCommandBase
    {
        [Fact]
        public async Task CreateCompanyCommand_Success()
        {
            // Arrange
            var name = "test3";
            var comment = "test3";
            var creationTime = DateTime.Now;
            var decisionMakerId = Guid.Empty;
            var level = Enums.LevelsEnum.Premium;

            // Act
            var companyId = await сompanyService.CreateCompany(
                new Company
                {
                    Name = name,
                    Comment = comment,
                    CreationTime = creationTime,
                    DecisionMakerId = decisionMakerId,
                    Level = level
                });

            // Assert
            Assert.NotNull(await сompanyService.GetCompaniesById(new List<Guid> { companyId }));
        }
    }
}
