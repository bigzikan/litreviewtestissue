﻿using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Tests.Common;
using Xunit;

namespace LitReviewTestIssue.Tests.Companies.Commands
{
    public class DeleteCompanyCommandTests : TestCommandBase
    {
        [Fact]
        public async Task DeleteCompanyCommand_FailOnWrongId()
        {
            // Arrange

            // Act            

            // Assert
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () =>
                await сompanyService.DeleteCompany(ServerDBContextFactory.CompanyId));
        }

        [Fact]
        public async Task DeleteCompanyCommand_Success()
        {
            // Arrange

            // Act            
            var companyId = await сompanyService.CreateCompany(
                new Company
                {
                    Name = "test4",
                    Comment = "test4",
                    CreationTime = DateTime.Now,
                    DecisionMakerId = Guid.Empty,
                    Level = Enums.LevelsEnum.Premium
                });

            // Assert
            Assert.True(await сompanyService.DeleteCompany(companyId));
        }
    }
}
