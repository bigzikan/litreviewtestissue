﻿using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Enums;
using LitReviewTestIssue.Tests.Common;
using Xunit;

namespace LitReviewTestIssue.Tests.Communications.Commands
{
    public class DeleteCommunicationCommandTests : TestCommandBase
    {
        [Fact]
        public async Task DeleteContactCommand_FailOnWrongId()
        {
            // Arrange            

            // Act            

            // Assert
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () =>
                await contactService.DeleteContact(ServerDBContextFactory.ContactId));
        }

        [Fact]
        public async Task DeleteContactCommand_Success()
        {
            // Arrange            
            var email = "test3";
            var phoneNumber = "1234";            

            var communication = new Communication
            {
                CompanyId = ServerDBContextFactory.CompanyId,
                ContactId = ServerDBContextFactory.ContactId,
                Email = email,
                PhoneNumber = phoneNumber,
                Type = TypeEnum.All
            };

            // Act            
            var contactId = await communicationService.CreateСommunication(communication);

            // Assert
            Assert.True(await communicationService.DeleteСommunication(contactId));
        }
    }
}
