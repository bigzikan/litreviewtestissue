﻿using LitReviewTestIssue.Data.Repository;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Tests.Common;
using Xunit;
using LitReviewTestIssue.Enums;

namespace LitReviewTestIssue.Tests.Communications.Commands
{
    public class UpdateCommunicationCommandTests : TestCommandBase
    {
        [Fact]
        public async Task UpdateCommunicationCommandTests_Success()
        {
            // Arrange
            var ContactRepository = new ContactRepository(context);
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;
            var email = "test3";
            var phoneNumber = "1234";

            var communication = new Communication
            {
                CompanyId = ServerDBContextFactory.CompanyId,
                ContactId = ServerDBContextFactory.ContactId,
                Email = email,
                PhoneNumber = phoneNumber,
                Type = TypeEnum.All
            };

            // Act
            var communicationId = await communicationService.CreateСommunication(communication);
            communication.Id = communicationId;
            communication.Email = "4321";

            // Assert
            Assert.True(await communicationService.UpdateCommunication(communication));
        }

        [Fact]
        public async Task UpdateCommunicationCommandTests_FailOnWrongId()
        {
            // Arrange
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;
            var decisionMakerId = Guid.Empty;
            var email = "test3";
            var phoneNumber = "1234";

            var communication = new Communication
            {
                CompanyId = ServerDBContextFactory.CompanyId,
                ContactId = ServerDBContextFactory.ContactId,
                Email = email,
                PhoneNumber = phoneNumber,
                Type = TypeEnum.All
            };

            // Act            
            await communicationService.CreateСommunication(communication);
            communication.Id = ServerDBContextFactory.CompanyId;

            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await communicationService.UpdateCommunication(communication));
        }
    }
}
