﻿using LitReviewTestIssue.Tests.Common;
using Xunit;
using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Enums;

namespace LitReviewTestIssue.Tests.Communications.Commands
{
    public class CreateCommunicationCommandTests : TestCommandBase
    {
        [Fact]
        public async Task CreateCommunicationCommand_Success()
        {
            // Arrange            
            var email = "test3";
            var phoneNumber = "1234";

            // Act
            var communication = new Communication
            {
                CompanyId = ServerDBContextFactory.CompanyId,
                ContactId = ServerDBContextFactory.ContactId,
                Email = email,
                PhoneNumber = phoneNumber,
                Type = TypeEnum.All
            };

            var communicationId = await communicationService.CreateСommunication(communication);

            // Assert
            Assert.NotNull(await communicationService.GetCommunicationsById(new List<Guid> { communicationId }));
        }
    }
}
