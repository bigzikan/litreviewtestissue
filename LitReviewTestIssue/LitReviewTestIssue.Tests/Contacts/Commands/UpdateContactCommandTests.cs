﻿using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Tests.Common;
using Xunit;
using LitReviewTestIssue.Enums;

namespace LitReviewTestIssue.Tests.Contacts.Commands
{
    public class UpdateContactCommandTests : TestCommandBase
    {
        [Fact]
        public async Task UpdateContactCommandTests_Success()
        {
            // Arrange
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;
            var email = "test3";
            var phoneNumber = "1234";

            var communication = new Communication
            {
                CompanyId = ServerDBContextFactory.CompanyId,
                ContactId = ServerDBContextFactory.ContactId,
                Email = email,
                PhoneNumber = phoneNumber,
                Type = TypeEnum.All
            };

            // Act
            var communicationId = await communicationService.CreateСommunication(communication);
            communication.Id = communicationId;       

            // Assert
            Assert.True(await communicationService.UpdateCommunication(communication));
        }

        [Fact]
        public async Task UpdateContactCommandTests_FailOnWrongId()
        {
            // Arrange
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;
            var decisionMakerId = Guid.Empty;

            var contact = new Contact
            {
                Name = name,
                Surname = name,
                MiddleName = name,
                CompanyId = ServerDBContextFactory.CompanyId,
                CreationTime = creationTime,
                IsDecisionMaker = false,
                JobTitle = jobTitle
            };

            // Act            
            await contactService.CreateContact(contact);
            contact.Id = ServerDBContextFactory.CompanyId;

            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () => await contactService.UpdateContact(contact));
        }
    }
}
