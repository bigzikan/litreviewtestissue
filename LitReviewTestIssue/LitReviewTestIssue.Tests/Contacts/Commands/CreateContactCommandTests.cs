﻿using LitReviewTestIssue.Tests.Common;
using Xunit;
using LitReviewTestIssue.Data.Models;

namespace LitReviewTestIssue.Tests.Contacts.Commands
{
    public class CreateContactCommandTests : TestCommandBase
    {
        [Fact]
        public async Task CreateContactCommand_Success()
        {
            // Arrange            
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;

            var contact = new Contact
            {
                Name = name,
                Surname = name,
                MiddleName = name,
                CompanyId = ServerDBContextFactory.CompanyId,
                CreationTime = creationTime,
                IsDecisionMaker = false,
                JobTitle = jobTitle
            };

            // Act
            var contactId = await contactService.CreateContact(contact);

            // Assert
            Assert.NotNull(await contactService.GetContactsById(new List<Guid> { contactId }));
        }
    }
}
