﻿using LitReviewTestIssue.Data.Models;
using LitReviewTestIssue.Tests.Common;
using Xunit;

namespace LitReviewTestIssue.Tests.Contacts.Commands
{
    public class DeleteCommunicationCommandTests : TestCommandBase
    {
        [Fact]
        public async Task DeleteContactCommand_FailOnWrongId()
        {
            // Arrange            

            // Act            

            // Assert
            await Assert.ThrowsAnyAsync<Exception>(async () =>
                await communicationService.DeleteСommunication(ServerDBContextFactory.ContactId));
        }

        [Fact]
        public async Task DeleteContactCommand_Success()
        {
            // Arrange
            var name = "test3";
            var jobTitle = "test3";
            var creationTime = DateTime.Now;

            var contact = new Contact
            {
                Name = name,
                Surname = name,
                MiddleName = name,
                CompanyId = ServerDBContextFactory.CompanyId,
                CreationTime = creationTime,
                IsDecisionMaker = false,
                JobTitle = jobTitle
            };

            // Act            
            var contactId = await contactService.CreateContact(contact);

            // Assert
            Assert.True(await contactService.DeleteContact(contactId));
        }
    }
}
